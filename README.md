# REQUIREMENTS

`docker` and `docker-compose`

# RUNNING

Run `docker-compose up -d`

The application lives at `http://localhost:8080`.

# TESTING

When the containers are running (for the sake of simplicity):

## Backend

* Tests: `docker-compose exec back pytest`
* Linting: `docker-compose exec back pycodestyle src`

## Frontend

* Tests: `docker-compose exec front npm run test:unit`
* Linting: `docker-compose exec front npm run lint`

# PLAYING

* Double click once to mark a cell as possibly having a mine.
* Double click twice to mark a cell as certainly having a mine (it can no longer be clicked).
* Double click again to remove the flag.
* Click normally to trigger a cell
