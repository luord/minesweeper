from app.api import GameView

from flask import Flask
from flask_cors import CORS


def create_app():
    app = Flask('app')
    CORS(app)

    app.add_url_rule('/game', view_func=GameView.as_view('game'))

    return app
