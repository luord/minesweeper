from itertools import product
import random

from flask import jsonify, abort, request
from flask.views import MethodView


class Game:
    def __init__(self, randomizer=random.randint):
        self.randomizer = randomizer

    def get(self):
        lines = self.randomizer(5, 100)
        mine_total = self.randomizer(lines, (lines**2//3)+1)

        mines = set()

        while mine_total:
            mine = self.randomizer(0, lines-1) * lines +\
                self.randomizer(0, lines-1)
            if mine not in mines:
                mine_total -= 1
            mines.add(mine)

        return {
            'lines': lines,
            'mines': mines,
            'revealed': {}
        }

    def patch(self, game, pos):
        return self._reveal(game, pos)

    def _reveal(self, game, pos):
        if str(pos) in game['revealed']:
            return game
        neighbors = self._get_neighbors(game['lines'], pos)
        mines = self._get_mines(neighbors, set(game['mines']))

        game['revealed'][str(pos)] = mines

        if mines:
            return game

        for cell in neighbors:
            game = self._reveal(game, cell)
        return game

    def _get_mines(self, cells, mines):
        return sum(1 for cell in cells if cell in set(mines))

    def _get_neighbors(self, lines, pos):
        pos = (pos//lines, pos % lines)
        coords = set(product((-1, 0, 1), repeat=2))
        neighbors = [tuple(sum(x) for x in zip(pos, c)) for c in coords]

        return [it[0] * lines + it[1] for it in neighbors if all(
            0 <= x < lines for x in it
        )]


class GameView(MethodView):
    def __init__(self, game=None):
        self.game = game or Game()

    def get(self):
        result = self.game.get()
        result['mines'] = list(result['mines'])

        return jsonify(result)

    def patch(self):
        game = request.json['game']
        pos = request.json['pos']
        pos = pos[0] * game['lines'] + pos[1]

        if pos in game['mines']:
            abort(401)

        result = self.game.patch(game, pos)
        return jsonify(result)
