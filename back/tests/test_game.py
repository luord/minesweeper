from unittest.mock import MagicMock, patch

import pytest

from app import create_app
from app.api import Game, GameView


@pytest.fixture
def app():
  return create_app()

@pytest.fixture
def neighbors_mock():
    with patch.object(Game, '_get_neighbors') as mock_neighbors:
        yield mock_neighbors

@pytest.fixture
def mines_mock():
    with patch.object(Game, '_get_mines') as mock_mines:
        yield mock_mines

@pytest.fixture
def game_data():
    return {'lines': 4, 'mines': {1, 11, 15, 6, 13, 3}, 'revealed': {}}

@pytest.fixture
def game():
    return Game()


def test_game_get():
  randoms = [
    6, 6, 0, 2, 3, 4, 5, 5, 3, 1, 2, 5, 3, 3
  ]
  expected_mines = {2, 22, 35, 19, 17, 21}

  randomizer = MagicMock(side_effect=randoms)

  game = Game(randomizer=randomizer)

  assert game.get() == {
    'lines': randoms[0],
    'mines': expected_mines,
    'revealed': {}
  }

  # Test repeated are ignored

  randoms[10] = 5
  randoms += [3, 5]

  expected_mines.remove(17)
  expected_mines.add(23)

  randomizer.side_effect = randoms

  assert game.get() == {
    'lines': randoms[0],
    'mines': expected_mines,
    'revealed': {}
  }

def test_game_get_request(app):
  expected = {'mines': [0]}

  game = MagicMock()
  game.get.return_value = expected

  app.add_url_rule('/test', view_func=GameView.as_view('test', game=game))

  result = app.test_client().get('/test')

  assert result.json == expected

def test_game_patch():
    expected = {'lines': 1, 'mines': [], 'revealed': {1: 2}}
    game = Game()

    with patch.object(game, '_reveal', return_value=expected) as mocked:
        result = game.patch({}, ())

        assert result == expected
        mocked.assert_called_once()
        mocked.assert_called_with({}, ())

def test_game_reveal_does_nothing_if_exists(game, game_data):
    data = {'revealed': {'0': 1}}

    assert game._reveal(game_data, 0) == {**game_data, **data}

def test_game_reveal_stops_if_mines_are_found(game, mines_mock, neighbors_mock, game_data):
    cell = 1
    mines_mock.return_value = mines = 2
    neighbors_mock.return_value = neighbors = [5]

    assert game._reveal(game_data, cell)['revealed'] == {str(cell): mines}
    mines_mock.assert_called_once_with(neighbors, set(game_data['mines']))
    neighbors_mock.assert_called_once_with(game_data['lines'], cell)

def test_game_reveal_is_called_on_neighbors_when_no_mines(game, mines_mock, neighbors_mock, game_data):
    cell = 1
    mines_mock.return_value = mines = 0
    neighbors_mock.return_value = neighbors = [5]

    assert game._reveal(game_data, cell)['revealed'] == {str(cell): mines, str(neighbors[0]): mines}
    assert mines_mock.call_count == 2
    assert neighbors_mock.call_count == 2
    neighbors_mock.assert_any_call(game_data['lines'], neighbors[0])

def test_game_get_mines(game, game_data):
    cells = list(game_data['mines'])[:3] + [0]
    assert game._get_mines(cells, game_data['mines']) == 3

def test_game_get_neighbors(game, game_data):
    result = game._get_neighbors(game_data['lines'], 0)
    assert set(result) == {1, 5, 4, 0}
    assert len(result) == 4

    cell = game_data['lines'] ** 2 - 1
    result = game._get_neighbors(game_data['lines'], cell)
    assert set(result) == {14, 11, 10, 15}
    assert len(result) == 4

    cell = 1
    result = game._get_neighbors(game_data['lines'], cell)
    assert set(result) == {0, 2, 4, 5, 6, cell}
    assert len(result) == 6

    cell = 14
    result = game._get_neighbors(game_data['lines'], cell)
    assert set(result) == {13, 15, 9, 10, 11, cell}
    assert len(result) == 6

    cell = 10
    result = game._get_neighbors(game_data['lines'], cell)
    assert set(result) == {5, 6, 7, 9, 11, 13, 14, 15, cell}
    assert len(result) == 9

def test_game_patch_request(app):
  expected = {'mines': [0]}

  data = {
    'game': {'mines': [1], 'lines': 2},
    'pos': [0, 0]
  }

  game = MagicMock()
  game.patch.return_value = expected

  app.add_url_rule('/test', view_func=GameView.as_view('test', game=game))

  result = app.test_client().patch('/test', json=data)

  assert result.json == expected
  game.patch.assert_called_with(data['game'], 0)

  result = app.test_client().patch('/test', json={
    'game': {'mines': [1], 'lines': 2},
    'pos': [0, 1]
  })

  assert result.status_code == 401
