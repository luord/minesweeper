export default {
  async handleClick (cell) {
    if (['mine', 'revealed'].indexOf(this.flagged[cell[0] * this.game.lines + cell[1]]) >= 0) {
      return
    }
    this.patchGame(cell)
  },
  async getGame () {
    try {
      let data = await fetch(process.env.VUE_APP_GAME_ENDPOINT)
      data = await data.json()

      this.game = Object.assign({}, data)
      this.flagged = Object.assign({})
    } catch (e) {}
  },
  async patchGame (cell) {
    try {
      let data = await fetch(
        process.env.VUE_APP_GAME_ENDPOINT,
        {
          method: 'PATCH',
          body: JSON.stringify({ game: this.game, pos: cell }),
          headers: { 'Content-Type': 'application/json' }
        }
      )
      data = await data.json()

      this.game = Object.assign({}, data)

      if (Object.keys(this.game.revealed).length + this.game.mines.length === this.game.lines ** 2) {
        alert('You win!')

        this.getGame()
        return
      }

      for (const pos of Object.keys(this.game.revealed)) {
        this.flagged[pos] = 'revealed'
      }
    } catch (e) {
      alert('You lose!')

      this.getGame()
    }
  },
  flag (cell) {
    const pos = cell[0] * this.game.lines + cell[1]

    const ops = {
      undefined: () => { this.flagged[pos] = 'mine' },
      mine: () => { this.flagged[pos] = 'mark' },
      mark: () => { delete this.flagged[pos] },
      revealed: () => { /**/ }
    }
    ops[this.flagged[pos]]()
  }
}
