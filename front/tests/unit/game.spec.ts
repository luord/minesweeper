import { enableFetchMocks } from 'jest-fetch-mock'
import { shallowMount } from '@vue/test-utils'
import flushPromises from 'flush-promises'

import Game from '@/components/game/Game.vue'
import methods from '@/components/game/methods'

enableFetchMocks()
window.alert = jest.fn()

describe('Game.vue', () => {
  test('Calls the appropriate methods when required', async () => {
    const methodSpies = {
      handleClick: jest.spyOn(methods, 'handleClick'),
      getGame: jest.spyOn(methods, 'getGame'),
      flag: jest.spyOn(methods, 'flag')
    }

    const wrapper = shallowMount(Game)

    expect(methodSpies.getGame).toHaveBeenCalled()

    await wrapper.setData({ game: { lines: 2 } })
    wrapper.find('td').trigger('click')

    expect(methodSpies.handleClick).toHaveBeenCalledWith([0, 0])

    wrapper.find('td').trigger('contextmenu')
    expect(methodSpies.flag).toHaveBeenCalledWith([0, 0])
  })

  test('Loads and displays data correctly', async () => {
    const data = { lines: 2, mines: [3], revealed: { 0: 1 } }
    fetchMock.mockResponseOnce(JSON.stringify(data))

    const wrapper = shallowMount(Game)

    await flushPromises()

    expect(wrapper.vm.game).toStrictEqual(data)
    expect(wrapper.findAll('tr').length).toBe(2)
    expect(wrapper.findAll('td').length).toBe(4)
    expect(wrapper.find('td').text()).toEqual('1')
  })

  test('handles click correctly', async () => {
    const methodSpies = {
      patchGame: jest.spyOn(methods, 'patchGame'),
      getGame: jest.spyOn(methods, 'getGame')
    }

    const wrapper = shallowMount(Game)
    await wrapper.setData({ flagged: { 1: 'mine' }, game: { lines: 2 } })

    await wrapper.find('.mine').trigger('click')

    expect(methodSpies.patchGame).not.toHaveBeenCalled()

    await wrapper.find('td[class=""]').trigger('click')
    expect(methodSpies.patchGame).toHaveBeenCalledWith([0, 0])
  })

  test('flags correctly', async () => {
    const wrapper = shallowMount(Game)
    expect(wrapper.find('td.revealed').exists()).toBeFalsy()

    await wrapper.setData({ game: { lines: 2 }, flagged: { 3: 'revealed' } })
    expect(wrapper.find('td.revealed').exists()).toBeTruthy()

    await wrapper.findAll('td')[1].trigger('contextmenu')
    expect(wrapper.vm.flagged).toStrictEqual({ 3: 'revealed', 1: 'mine' })
    expect(wrapper.find('td.mine').exists()).toBeTruthy()

    await wrapper.findAll('td')[1].trigger('contextmenu')
    expect(wrapper.vm.flagged).toStrictEqual({ 3: 'revealed', 1: 'mark' })
    expect(wrapper.find('td.mark').exists()).toBeTruthy()

    await wrapper.findAll('td')[1].trigger('contextmenu')
    expect(wrapper.vm.flagged).toStrictEqual({ 3: 'revealed' })

    await wrapper.get('td.revealed').trigger('contextmenu')
    expect(wrapper.vm.flagged).toEqual({ 3: 'revealed' })
  })

  test('Patches correctly', async () => {
    const getGame = jest.spyOn(methods, 'getGame')

    const wrapper = shallowMount(Game)
    await wrapper.setData({ game: { lines: 2 } })

    fetchMock.resetMocks()
    fetchMock.mockRejectOnce()

    wrapper.find('td').trigger('click')
    await flushPromises()

    expect(fetchMock.mock.calls[0][0]).toEqual(process.env.VUE_APP_GAME_ENDPOINT)
    expect(fetchMock.mock.calls[0][1]).toStrictEqual({
      method: 'PATCH',
      body: JSON.stringify({ game: wrapper.vm.game, pos: [0, 0] }),
      headers: { 'Content-Type': 'application/json' }
    })
    expect(window.alert).toHaveBeenCalledWith('You lose!')
    expect(getGame).toHaveBeenCalled()

    jest.clearAllMocks()
    fetchMock.resetMocks()

    const gameData = {
      lines: 4,
      mines: [13, 8, 0, 5, 2],
      revealed: { 1: 1, 3: 3, 4: 1, 6: 0, 7: 1, 9: 2, 10: 1, 11: 2, 12: 2, 14: 0, 15: 1 }
    }

    fetchMock.mockResponseOnce(JSON.stringify(gameData))
    wrapper.find('td').trigger('click')

    await flushPromises()

    expect(window.alert).toHaveBeenCalledWith('You win!')
    expect(getGame).toHaveBeenCalled()

    jest.clearAllMocks()
    fetchMock.resetMocks()

    gameData.lines = 5
    const expected = Object.keys(gameData.revealed).reduce((o, key) => ({ ...o, [key]: 'revealed' }), {})

    fetchMock.mockResponseOnce(JSON.stringify(gameData))
    wrapper.find('td').trigger('click')

    await flushPromises()

    expect(wrapper.vm.flagged).toEqual(expected)
  })
})
